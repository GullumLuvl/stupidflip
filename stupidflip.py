#!/usr/bin/env python3

# Copyright © 2023 GullumLuvl
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

"""
Pitifully basic transliteration.


Mini language:

input letters -> result

input letters: X>Y:Z with:
X = to replace 
Y = if before
Z = if after
when both before and after given, both conditions must be met (logical 'AND')

'!' is the negation of ':',
'/' is the negation of '>'

special character codes:

* = anything, including nothing (end or beginning of word)
. = any character (non empty)
% = a word boundary (\b in regex)
& = the same character as matched previously: e.g. 'a§&' would match a followed by 2 identical consonnants.
    allowed in the replacement string

Custom special characters can be defined in the script, conventionally:
_ = any vowel (a,e,i,o,u,y) or compound vowel ('au', 'on', etc)
§ = any consonant
Their definition can be omitted and a default one will be used.
"""


import os.path as op
import unicodedata
from functools import partial
import logging
logger = logging.getLogger(__name__)


FR_SCRIPT = op.join(op.dirname(op.abspath(__file__)), 'fr.script')
FR_FIRSTNAMES_SCRIPT = op.join(op.dirname(op.abspath(__file__)), 'fr_firstnames.script')

# Useful defaults for French
FR_PROTECT = {'æ': '4', 'Æ': '4',
              'œ': '8', 'Œ': '8',
              'ç': '9', 'Ç': '9',
              'ï': '1', 'Ï': '1',
              'é': '6', 'É': '6',
              'è': '3', 'È': '3',
              'ê': '3', 'Ê': '3'}

VOWELS = set('aeiou13684œæ') # | set('@°01¢üéèɑ̃əøɛ̃ωɔ̃')
CONSON = set('bcdfghjklmnpqrstvwxyz79µ')

SPECIAL = {'_': VOWELS, '§': CONSON}


def dedup_conson(text):
    newtext = ''
    previous = ''
    for char in text:
        if char.lower() != previous or char.lower() not in CONSON:
            newtext += char
            previous = char.lower()
    return newtext


def check_together(text, i, matching, shift=None):
    r = True
    shift = 0
    previous = text[i] if (text and i>=0) else ''
    capture = ''  # Last character corresponding to '&'
    for k, c in enumerate(matching, start=i+1):
        if c == '%':
            r &= (k >= len(text) or not text[k].isalnum())
            return r, shift, capture
        if k >= len(text):
            r &= (c=='*')
            continue
        shift += 1
        if c == '*':
            continue  # any character is ok.
        if c == '&':
            if text[k].lower() != previous:
                return False, shift-1, capture
            capture = previous
            continue
        previous = text[k].lower()
        if c == '.':
            r &= previous.isalnum()  # any alphanumeric character is ok.
        elif c == '_':
            r &= (previous in VOWELS)
        elif c == '§':
            r &= (previous in CONSON)
        else:
            r &= (previous == c.lower())
    return r, shift, (capture or previous)  # Either the character matched by '&', or the last one.


def check_followed_by(text, i, matching, shift=0):
    """Return a pair (bool, int) indicating if it matched, and the matched length"""
    i += shift
    if matching == '*':
        return True, 0, ''
    if matching == '.':
        return (i+1 < len(text) and text[i+1].isalnum()), 0, ''
    if matching == '&':
        return (i+1 < len(text) and text[i+1].lower() == text[i].lower()), 0, text[i].lower()
    #if set(matching).intersection('*.&'):
    #    raise ValueError('Invalid syntax, *.& should be alone in the group.')

    if '%' in matching and (i+1 >= len(text) or not text[i+1].isalnum()):
        return True, 0, ''

    valid_set = set(c for c in matching if c not in '§_%&')
    if '§' in matching:
        valid_set |= CONSON
    if '_' in matching:
        valid_set |= VOWELS

    return (i+1 < len(text) and text[i+1].lower() in valid_set), 0, ''


def check_not_followed_by(text, i, matching, shift=0):
    """None of the given characters is following"""
    i += shift
    if matching == '*':
        return False, 0, ''
    if matching == '.':
        return (i+1 >= len(text) or not text[i+1].isalnum()), 0, ''
    if matching == '&':
        return (i+1 >= len(text) or text[i+1].lower() != text[i].lower()), 0, text[i].lower()

    if '%' in matching and (i+1 >= len(text) or not text[i+1].isalnum()):
        return False, 0, ''

    reject_set = set(c for c in matching if c not in '§_%&')
    if '§' in matching:
        reject_set |= CONSON
    if '_' in matching:
        reject_set |= VOWELS

    return (i+1 >= len(text) or text[i+1].lower() not in reject_set), 0, ''


def check_preceded_by(text, i, matching, shift=None):
    if matching == '*':
        return True, 0, ''
    if matching == '.':
        return (i > 0 and text[i-1].isalnum()), 0, ''
    if matching == '&':
        return (i > 0 and text[i-1].lower() == text[i].lower()), 0, text[i].lower()
    #if set(matching).intersection('*.&'):
    #    raise ValueError('Invalid syntax, *.& should be alone in the group.')

    if '%' in matching and (i==0 or not text[i-1].isalnum()):
        return True, 0, ''

    valid_set = set(c for c in matching if c not in '§_%&')
    if '§' in matching:
        valid_set |= CONSON
    if '_' in matching:
        valid_set |= VOWELS

    return (i > 0 and text[i-1].lower() in valid_set), 0, ''


def check_not_preceded_by(text, i, matching, shift=None):
    if matching == '*':
        return False, 0, ''
    if matching == '.':
        return (i == 0 or not text[i-1].isalnum()), 0, ''
    if matching == '&':
        return (i == 0 or text[i-1].lower() != text[i].lower()), 0, text[i].lower()

    if '%' in matching and (i == 0 or not text[i-1].isalnum()):
        return False, 0, ''

    reject_set = set(c for c in matching if c not in '§_%&')
    if '§' in matching:
        reject_set |= CONSON
    if '_' in matching:
        reject_set |= VOWELS

    return (i == 0 or text[i-1].lower() not in reject_set), 0, ''


class Checker:
    def __init__(self, together='', after='', before='', not_follow=False, not_preced=False):
        self.together = together
        self.after = after
        self.before = before
        self.not_follow = not_follow
        self.not_preced = not_preced

        checks = []
        if before:
            # At the beginning of the check list, because it must not be affected
            # by the shifting
            if not_preced:
                checks.append(partial(check_not_preceded_by, matching=before))
            else:
                checks.append(partial(check_preceded_by, matching=before))
        if together:
            checks.append(partial(check_together, matching=together))
        if after:
            if not_follow:
                checks.append(partial(check_not_followed_by, matching=after))
            else:
                checks.append(partial(check_followed_by, matching=after))
        self.checks = checks

    def __call__(self, text, i):
        shift = 0
        self.failed = None
        self.capture = ''
        for n, check in enumerate(self.checks):
            r, nextshift, capture = check(text, i, shift=shift)
            if r:
                logger.debug('i=%d %r #.%d shift=%d %s(%r) %r nextshift=%d',
                     i, text[i], n, shift, check.func.__name__, check.keywords['matching'], r, nextshift)
            shift += nextshift
            if not r:
                self.shift = 1
                self.failed = n
                return False
            if capture:
                self.capture = capture
        self.shift = shift+1
        return True


def index_conversions(conversions):
    idx = {}  # first character : next checks
    for source, result in conversions:
        not_follow, not_preced = False, False
        build_lists = ['', '', '']
        item = 0  # To which build list we add the characters
        next_valid_items = set((1,2))
        if source[0] == '%':
            raise NotImplementedError('Match start word boundary ("%")')
        elif source[0] == '&':
            raise ValueError('Match start is previous match ("&")')
        for c in source[1:]:
            if c == ':':
                item = 1
            elif c == '!':
                item = 1
                not_follow = True
            elif c == '>':
                item = 2
            elif c == '/':
                item = 2
                not_preced = True
            else:
                build_lists[item] += c
                #TODO: if the code is '*' or '.', no other symbol is accepted.
                continue
            try:
                next_valid_items.remove(item)
            except KeyError:
                #FIXME: this would be meaningful to chain several follows rules (just shift and add another follow check).
                raise ValueError('Invalid syntax: operands %s or %s can only be used once' % (':>'[item-1], '!/'[item-1]))
        char_checkers = idx.setdefault(source[0].lower(), list())
        char_checkers.append((Checker(*build_lists, not_follow=not_follow, not_preced=not_preced),
                              result))
    # Case-insensitivity:
    for char in list(idx):
        idx[char.upper()] = idx[char]

    return idx


def _try_split_command(cmd, err_msg=''):
    try:
        source, result = cmd.split('→', 1)
    except ValueError:
        try:
            source, result = cmd.split('->', 1)
        except ValueError as err:
            err.args = (err_msg,)
            raise
    return source.strip(), result.strip()


class StupidTranslit:

    @property
    def protect(self):
        return self._protect

    @protect.setter
    def protect(self, protect):
        self._protect = protect if protect else None
        self._pre_translate = None
        if protect:
            self._pre_translate = {ord(c): ord(v) for c,v in protect.items()}

    def __init__(self, conversions, special=None, protect=None):
        self.conversions = conversions
        self.idx = index_conversions(conversions)
        self.protect = protect # Conversions to apply to single characters before the transliteration
        self.special = special # Special variables used in pattern matching: e.g. '_' for vowels and '§' for consonants

    @classmethod
    def fromscript(cls, scriptname):
        with open(scriptname) as f:
            scripttext = f.read()
        return cls.fromcode(scripttext)
    
    @classmethod
    def fromcode(cls, scripttext):
        conversions = []
        protect = {}
        special = {}
        for n, line in enumerate(scripttext.splitlines()):
            line = line.strip()
            cmd, _, comment = line.partition('#')
            #TODO:
            # check that the symbol '#' was not escaped?
            cmd = cmd.strip()
            if '=' in cmd:
                code, equivalent = cmd.split('=', 1)
                code = code.strip()
                if len(code) != 1:
                    raise ValueError('Special code should a single character! line #%d' % n)
                special[code] = equivalent.strip()
            elif cmd.startswith('!'):
                source, result = _try_split_command(cmd[1:], 'Protect command line #%d should be formatted as "! X -> R"' % n)
                protect[source] = result
                protect[source.upper()] = result.upper()
            elif cmd:
                source, result = _try_split_command(cmd, 'Command line #%d should be formatted as "X>Y:Z -> R"' % n)
                conversions.append((source, result))

        return cls(conversions, special, protect)


    def prepare_text(self, text, norm='NFKD'):
        if self.protect is None:
            return text
        newtext = text.translate(self._pre_translate)
        # NFKD decomposes accentuated characters, so that accents can be stripped off.
        return unicodedata.normalize(norm, newtext).encode('ascii', 'ignore').decode('utf-8')

    def prepare_series(self, textseries, norm='NFKD'):
        if self.protect is None:
            return textseries
        newtext = textseries.str.translate(self._pre_translate)
        return newtext.str.normalize(norm).str.encode('ascii', 'ignore').str.decode('utf-8')

    def __call__(self, text):
        return self.encode(self.prepare_text(text))

    def encode(self, text):
        newtext = ''
        i = 0
        while i < len(text):
            element = text[i]
            conversions = [conv for initchar in [text[i], '_', '§', '.', '*']
                                for conv in self.idx.get(initchar, [])]
            for check, newvalue in conversions:
                if check(text, i):
                    element = newvalue.replace('&', check.capture)
                    shift = check.shift
                    logger.debug('OK: check("%s%s:%s>%s",%s,%s)', text[i], check.together, check.after, check.before, check.not_follow, check.not_preced)
                    break
            else:
                shift = 1
            newtext += element
            assert shift > 0, 'text[%d]=%r new=%r' % (i, text[i], element)
            i += shift
        return dedup_conson(newtext).lower()


def main():
    from sys import stdin
    import argparse as ap
    logging.basicConfig()
    parser = ap.ArgumentParser(description=__doc__,
                               formatter_class=ap.RawDescriptionHelpFormatter)
    parser.add_argument('text', nargs='*')
    parser.add_argument('-s', '--script', default='fr',
                        help='transliteration file without extension [%(default)s]')
    parser.add_argument('-d', '--debug', action='store_true')
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
    if args.script == '-':
        translit = StupidTranslit.fromcode(stdin.read())
    else:
        scriptfile = op.join(op.dirname(op.abspath(__file__)), args.script + '.script')
        translit = StupidTranslit.fromscript(scriptfile)

    texts = args.text if args.text else [line.strip() for line in stdin]
    for text in texts:
        print(translit(text))


if __name__ == '__main__':
    main()
