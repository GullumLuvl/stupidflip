Pitifully basic transliteration:

convert characters based on rules on the preceding and following characters.


## Python usage

1. Create a transliterator instance from a transliteration script as configuration:

    ```python3
    translit = StupidTranslit.fromscript(FR_SCRIPT)
    ```

2. transliterate text:

        translit("innocent")
        >>> 'inos@'


## Command line usage

    ./stupidflip.py --script fr "innocent"

Also see `--help`.

## Syntax of the transliteration script

Each line is a single rule, and everything after '#' is a comment.

A rule is build as `X>Y:Z → R` (`→` or `->` are valid), where:

- X is the series of characters to replace
- Y is the set of characters matched against one *single* preceding character
- Z is the set of characters matched against one *single* following character
- R is the series of replacement characters.

- `!` negates ':', meaning *not* followed by any of Z
- `/` negates '>', meaning *not* preceded by any of Y

Rules are checked in the order they are given in the script, and the first matching one is applied.

Special lines:

* rules preceded by `!` are applied before everything else
  (before encoding normalization).
* `V = W` is a special character definition (e.g. to define vowels `_ = aeiouy`).

## NOTES

- The default class deduplicates consonants as a final processing.


