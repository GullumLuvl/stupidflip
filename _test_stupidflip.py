#!/usr/bin/env python3


from stupidflip import *
import pytest
logger.setLevel(logging.DEBUG)


together_input_outputs = [ # text, characters to match, result_has_matched, result_shift
        ("",  "%", True, 0),
        ("",  "*", True, 0),
        ("",  ".", False, 0),
        ("",  "a", False, 0),
        ("",  "_", False, 0),
        ("",  "§", False, 0),
        (" ", "%", True, 0),
        (" ", "*", True, 1),
        (" ", ".", False, 0),
        (" ", "a", False, 0),
        (" ", "_", False, 0),
        (" ", "§", False, 0),
        ("a", "%", False, 0),
        ("a", "*", True, 1),
        ("a", "a", True, 1),
        ("a", "_", True, 1),
        ("a", ".", True, 1),
        ("a", "b", False, 1),
        ("a", "§", False, 1),
        ("b", "%", False, 0),
        ("b", "*", True, 1),
        ("b", "b", True, 1),
        ("b", "§", True, 1),
        ("b", ".", True, 1),
        ("b", "a", False, 1),
        ("b", "_", False, 1),
        ("ab", "ab",  True, 2),
        ("ab", "ab%", True, 2),
        ("ab", "_b",  True, 2),
        ("ab", "a§",  True, 2),
        ("ab", "a§%", True, 2),
        ("ab", "_§",  True, 2),
        ("ab", "a%",  False, 1),
        ("aa", ".&", True, 2)]

followed_input_outputs = [in_out for in_out in together_input_outputs if len(in_out[1])==1]
followed_input_outputs += [
        ("", ".%", True, 0),
        ("", "_§", False, 0),
        ("a", "aeiouy136", True, 0),
        ("a", "eiouy136", False, 0)
        ]

not_followed_input_outputs = [
        ("", "%", False, 0),
        ("", "*", False, 0),
        ("", "a", True,  0),
        ("", ".", True,  0),
        ("", "_", True,  0),
        ("", "§", True,  0),
        (" ", "%", False, 0),
        (" ", "*", False, 0),
        (" ", "a", True,  0),
        (" ", ".", True,  0),
        (" ", "_", True,  0),
        (" ", "§", True,  0),
        ("a", "%", True,  0),
        ("a", "*", False, 0),
        ("a", "a", False, 0),
        ("a", "b", True,  0),
        ("a", "_", False, 0),
        ("a", "§", True,  0),
        ("a", "eiuoy136", True, 0),
        ("a", "%eiuoy136", True, 0),
        ("a", "aeiuoy136", False, 0),
        ("a", "_§", False, 0),
        ("a", "§_", False, 0)]

@pytest.mark.parametrize('input_output', together_input_outputs)
def test_check_together(input_output):
    text, matching, expected_matched, expected_shift = input_output
    matched, shift, capture = check_together(text, -1, matching)
    print('check(%r, , %r):' % (text, matching), matched, shift, repr(capture))
    assert matched == expected_matched
    if expected_matched:
        assert shift == expected_shift
        if '&' in matching:
            assert capture


@pytest.mark.parametrize('input_output', followed_input_outputs)
def test_check_followed_by(input_output):
    text, matching, expected_matched, _ = input_output
    matched, shift, _ = check_followed_by(text, -1, matching)
    print('check(%r, , %r):' % (text, matching), matched, shift)
    assert matched == expected_matched
    if expected_matched:
        assert shift == 0

@pytest.mark.parametrize('input_output', not_followed_input_outputs)
def test_check_not_followed_by(input_output):
    text, matching, expected_matched, _ = input_output
    matched, shift, _ = check_not_followed_by(text, -1, matching)
    print('check(%r, , %r):' % (text, matching), matched, shift)
    assert matched == expected_matched
    if expected_matched:
        assert shift == 0

#def test_check_not_preceded_by(input_output):
    #TODO

class Test_Checker:
    def test_3checks(self):
        check = Checker('b', '_', '_')
        assert len(check.checks) == 3
        assert check('axba', 1) is True, 'Failed at %s' % check.failed
        assert check.shift == 2, 'Successful test, shift by two'
        assert check('axbb', 1) is False
        assert check.shift == 1, 'Failed test, should only shift by one'
    def test_check_order_is_together_after_before(self):
        check = Checker('t', 'after', 'befor')
        c0, c1, c2 = check.checks
        assert c0.func.__name__ == 'check_preceded_by'
        assert c0.keywords['matching'] == 'befor'
        assert c1.func.__name__ == 'check_together'
        assert c1.keywords['matching'] == 't'
        assert c2.func.__name__ == 'check_followed_by'
        assert c2.keywords['matching'] == 'after'
    def test_2checks(self):
        check = Checker('', '_', '_')
        assert len(check.checks) == 2
        assert check('axa', 1) is True
        assert check.shift == 1
        assert check('axb', 1) is False
        assert check.shift == 1
    def test_check_repeat_together(self):
        check = Checker('&', '', '')
        assert len(check.checks) == 1
        for c in 'abcdefghijklmnopqrstuvwxyz1368':
            assert check(c+c, 0) is True
            assert check.shift == 2
            assert check.capture == c
            assert check(c+c.upper(), 0) is True
            assert check(c+('a' if c!='a' else 'b'), 0) is False
            assert check.shift == 1

    def test_check_repeat_follow(self):
        check = Checker('', '&', '')
        assert len(check.checks) == 1
        for c in 'abcdefghijklmnopqrstuvwxyz1368':
            assert check(c+c, 0) is True
            assert check.shift == 1
            assert check.capture == c
            assert check(c+c.upper(), 0) is True
            assert check(c+('a' if c!='a' else 'b'), 0) is False
            assert check.shift == 1

class Test_simple_StupidTranslit:
    def setup_method(self):
        script = """
                 b:_>_ → x
                 """
        self.translit = StupidTranslit.fromcode(script)

    def test_fromcode(self):
        idx = self.translit.idx
        print(idx)
        assert 'b' in idx
        assert len(idx['b']) == 1
        cmd0 = idx['b'][0]
        assert cmd0[1] == 'x'
        assert isinstance(cmd0[0], Checker)

    def test_fromcode2(self):
        translit = StupidTranslit.fromcode("d:% →")
        d_checks = translit.idx['d']
        assert len(d_checks) == 1
        d_check, d_result = d_checks[0]
        assert d_result == ''
        assert d_check.together == ''
        assert d_check.after == '%'
        assert d_check.before == ''
        assert len(d_check.checks) == 1
        d_check_follow = d_check.checks[0]
        assert d_check_follow.func.__name__ == 'check_followed_by'
        assert d_check_follow.keywords['matching'] == '%'
        assert d_check('ad', 1) is True
        assert d_check('ad ', 1) is True
        assert d_check('da', 0) is False

    def test_prepare_text(self):
        translit = self.translit
        translit.protect = FR_PROTECT
        prepared = translit.prepare_text('ç ä é è ï a e i œ aha')
        assert prepared == '9 a 6 3 1 a e i 8 aha'

        prepared = translit.prepare_text('Ç Ä É È Ï A E I Œ AHA')
        assert prepared == '9 A 6 3 1 A E I 8 AHA'


EXPECTED_FRANCOSONE = [
        ('abba', 'aba'),
        ('ça', 'sa'),
        ('aça', 'asa'),
        ('apha', 'afa'),
        ('aha', 'aa'),
        ('aci', 'asi'),
        ('aca', 'aka'),
        ('adja', 'aja'),
        ('agui', 'agi'),
        ('agi', 'aji'),
        ('aga', 'aga'),
        ('agna', 'aµa'),
        ('ailla', 'aya'),
        ('illa', 'ila'),
        ('billa', 'bila'), # somewhat arbitrary choice
        ('bille', 'biy'),
        ('bill', 'bil'),
        ('aqua', 'aka'),
        ('aqa', 'aka'),
        ('assa', 'asa'),
        ('asci', 'asi'),
        ('asa', 'aza'),
        ('sa', 'sa'),
        ('at', 'a'),
        ('at ', 'a '),
        ('ad', 'ad'),
        ('ad ', 'ad '),
        ('axci', 'axi'),
        ('axca', 'axka'),
        ('axça', 'axa'),
        ('baib', 'b3b'),
        ('baïb', 'baib'),
        ('beib', 'b3b'),
        ('beïb', 'b3ib'),
        ('beil', 'b3y'),
        ('beilla', 'b3ya'),
        ('bel', 'b3l'),
        ('bett', 'b3t'),
        ('berr', 'b3r'),
        ('bella', 'b3la'),
        ('abes', 'ab'),
        ('abe', 'ab'),
        ('amba', '@ba'),
        ('ampa', '@pa'),
        ('anda', '@da'),
        ('anna', 'ana'),
        ('ana', 'ana'),
        ('emba', '@ba'),
        ('empa', '@pa'),
        ('enda', '@da'),
        ('enna', '3na'),  # ambiguous, could be '6na' as well...
        ('ena', 'ena'),
        ('jean', 'j@'),
        ('jeanne', 'jan'),
        ('paon', 'p@'),
        ('paonne', 'paon'), # FIXME
        ('beab', 'bab'),    # dunno either
        ('bault', 'bo'),
        ('baut', 'bo'),
        ('baub', 'bob'),
        ('beau', 'bo'),
        ('beub', 'beb'),
        ('bœf', 'bef'),
        ('bœuf', 'bef'),
        ('aïb', 'aib'),
        #('frieda', 'frida'),
        ('bin', 'b1'),
        ('bun', 'b1'),
        ('bain', 'b1'),
        ('bein', 'b1'),
        ('boi', 'bωa'),
        ('boib', 'bωab'),
        ('boia', 'boia'),
        ('bon', 'b¢'),
        ('bong', 'b¢g'),
        ('bona', 'bona'),
        ('bomba', 'b¢ba'),
        ('boub', 'bub'),
        ('bub', 'büb'),
        ('magali', 'magali'),
        ('karim', 'karim'),
        ('carim', 'karim'),
        ('patrick', 'patrik'),
        ('anaïs', 'anais'),
        ('tiphaine', 'tif3n'),
        ('charles', '7arl'),
        ('chloé', 'klo6'),
        ('mahaut', 'mao'),
        ('josiane', 'jozian'),
        ('bille', 'biy'),
        ('bertille', 'b3rtiy'),
        ('madeleine', 'madel3n'),
        ('marguerite', 'margerit'),
        ('joseph', 'joz3f'),
        ('lucie', 'lüsi'),
        ('alphonsine', 'alf¢sin'),
        ('denyse', 'deniz'),
        ('zyneb', 'zin3b'),
        ('innocent', 'inos@')]
        #'denyse' should become 'deniz', not 'd@is'




class Test_Francosone:
    """Transliteration to french pronunciation"""
    def setup_method(self):
        self.francosone = StupidTranslit.fromscript(FR_SCRIPT)

    @pytest.mark.parametrize('text,expected', EXPECTED_FRANCOSONE)
    def test_expected(self, text, expected):
        francosone = self.francosone
        converted = francosone(text)

        assert converted == expected

